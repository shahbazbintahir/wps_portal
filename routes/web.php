<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::put('/profile/changeMode', 'ProfileController@changeMode')->name('profile.changeMode');
Route::post('/profile/changePassword', 'ProfileController@changePassword')->name('profile.changePassword');
Route::get('/profile/getBrands', 'ProfileController@getBrands')->name('profile.getBrands');
Route::post('/profile/switchBrand', 'ProfileController@switchBrand');

Route::get('admin/restore', 'AdminController@restore')->name('admin.restore');
Route::resource('/admin', 'AdminController');
Route::put('admin/status/{user}', 'AdminController@updateStatus');

Route::get('employee/restore', 'EmployeeController@restore')->name('employee.restore');
Route::resource('/employee', 'EmployeeController');
Route::put('employee/status/{user}', 'EmployeeController@updateStatus');

Route::group(['middleware' => ['Admin']], function () {
    Route::resource('/role', 'RoleController');
    Route::post('role/authorization/{role}', 'RoleController@updateAuthorization')->name('roleAuthorization');
});
