document.addEventListener('DOMContentLoaded', function(e) {

    //View all manager in datatabale
    $('#role_table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: base_url+"/role",
        },
        columns: [
            { "data": "DT_RowIndex", "name": "DT_RowIndex", orderable: false, searchable: false},
            { data: 'role_name',name: 'role_name' },
            { data: 'action',name: 'action', orderable: false },
        ]
    }).columns.adjust().draw();
});
