<?php

use App\Model\Profile;
use Illuminate\Database\Seeder;
use App\Model\Role;
use App\Model\User;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mgmt_user_role')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Profile::truncate();
        User::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $superadminRole = Role::where('role_name','Admin')->first();
        $userRole = Role::where('role_name','Employee')->first();

        $superadmin = User::create(['username' => 'Sanaullah Khan Akhundzada', 'email' => 'akhund.sana@gmail.com','password' => Hash::make('123456789'),]);
        $superadmin->roles()->attach($superadminRole);
        $superadmin = User::create(['username' => 'Shahbaz Bin Tahir', 'email' => 'shahbazbintahir@gmail.com','password' => Hash::make('123456789'),]);
        $superadmin->roles()->attach($superadminRole);

        $user = User::create(['username' => 'Employee', 'email' => 'employee@vccs.com','password' => Hash::make('123456789'),]);
        $user->roles()->attach($userRole);

    }
}
